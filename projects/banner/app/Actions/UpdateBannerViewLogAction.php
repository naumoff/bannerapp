<?php

namespace App\Actions;

use App\Database;

class UpdateBannerViewLogAction
{
    private Database $database;

    public function __construct()
    {
        $this->database = Database::getInstance();
    }

    /**
     * @param int $id
     * @param array $updatedValues
     * @return void
     */
    public function handle(int $id, array $updatedValues): void
    {
        $this->database->updateRecordById('banner_views', $id, $updatedValues);
    }
}