<?php

namespace App\Actions;

use App\Database;
use App\DTOs\BannerViewDTO;

readonly class InsertBannerViewLogAction
{
    private Database $database;

    public function __construct()
    {
        $this->database = Database::getInstance();
    }

    /**
     * @param BannerViewDTO $bannerView
     * @return void
     */
    public function handle(BannerViewDTO $bannerView): void
    {
        $this->database->insert('banner_views', $bannerView->toArray());
    }
}