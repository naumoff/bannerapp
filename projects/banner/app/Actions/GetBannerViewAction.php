<?php

namespace App\Actions;

use App\DTOs\BannerViewDTO;

class GetBannerViewAction
{
    /**
     * @return BannerViewDTO
     */
    public function handle(): BannerViewDTO
    {
        $userIp = $this->getUserIp();
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        $currentPath = $_SERVER['REQUEST_URI'];

        return new BannerViewDTO($userIp, $userAgent, $currentPath, 1);
    }

    /**
     * @return string
     */
    private function getUserIp(): string
    {
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $userIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $userIP = $_SERVER['REMOTE_ADDR'];
        }

        return $userIP;
    }
}