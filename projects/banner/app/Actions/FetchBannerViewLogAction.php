<?php

namespace App\Actions;

use App\Database;
use App\DTOs\BannerViewDTO;

readonly class FetchBannerViewLogAction
{
    private Database $database;

    public function __construct()
    {
        $this->database = Database::getInstance();
    }

    /**
     * @param BannerViewDTO $bannerViewDTO
     * @return array|null[]
     */
    public function handle(BannerViewDTO $bannerViewDTO): array
    {
        $searchParams = [
            'ip_address' => $bannerViewDTO->ipAddress,
            'page_url' => $bannerViewDTO->pageUrl,
        ];

        $result = $this->database->findRecordByColumnValues('banner_views', $searchParams);

        if ($result) {
            $id = $result['id'];
            $updatesValues = ['views_count' => $result['views_count']];

            return [$id, $updatesValues];
        }

        return [null, null];
    }
}