<?php

namespace App;

class Session
{
    public static function write(string $key, mixed $value): void
    {
        $_SESSION[$key] = $value;
    }

    public static function read(string $key): mixed
    {
        return $_SESSION[$key] ?? null;
    }
}