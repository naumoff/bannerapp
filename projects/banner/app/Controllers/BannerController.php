<?php

namespace App\Controllers;

use App\Services\UpdateBannerLogService;
use App\View;

readonly class BannerController
{
    private UpdateBannerLogService $bannerLogService;

    public function __construct() {
        $this->bannerLogService = new UpdateBannerLogService();
    }

    /**
     * @param string $htmlFile
     * @return void
     */
    public function renderHtmlFile(string $htmlFile): void
    {
        $bannerFile = match ($htmlFile) {
            'index1.html' => 'banner1.jpg',
            'index2.html' => 'banner2.jpg',
            default => null,
        };

        if ($bannerFile) {
            $this->bannerLogService->handle();
        }

        View::renderHtml($htmlFile, ['fileName' => $bannerFile]);
    }
}