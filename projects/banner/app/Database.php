<?php

namespace App;

use PDOException;
use PDO;

class Database
{
    private string $host;
    private string $username;
    private string $password;
    private string $database;
    private string $charset;

    private PDO $pdo;
    private static Database|null $instance = null;

    private function __construct()
    {
        $this->setDbCredentials();
        $dsn = "mysql:host=$this->host;dbname=$this->database;charset=$this->charset";

        try {
            $this->pdo = new PDO($dsn, $this->username, $this->password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die("DB connection error: " . $e->getMessage());
        }
    }

    private function setDbCredentials(): void
    {
        $this->host = $_ENV['DB_HOST'];
        $this->username = $_ENV['DB_USERNAME'];
        $this->password = $_ENV['DB_PASSWORD'];
        $this->charset = $_ENV['DB_CHARSET'];
        $this->database = $_ENV['DB_DATABASE'];
    }

    /**
     * @return Database
     */
    public static function getInstance(): Database
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param string $table
     * @param array $data
     * @return bool
     */
    public function insert(string $table, array $data): bool
    {
        $columns = implode(', ', array_keys($data));
        $values = ':' . implode(', :', array_keys($data));

        $sql = "INSERT INTO $table ($columns) VALUES ($values)";

        try {
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($data);

            return true; // success
        } catch (PDOException $e) {
            die("Insert error: " . $e->getMessage());
        }
    }

    /**
     * @param string $table
     * @param array $columns
     * @return mixed
     */
    public function findRecordByColumnValues(string $table, array $columns): mixed
    {
        if (empty($columns)) {
            return null;
        }

        $conditions = [];
        $params = [];

        foreach ($columns as $column => $value) {
            $conditions[] = "$column = :$column";
            $params[":$column"] = $value;
        }

        $sql = "SELECT * FROM $table WHERE " . implode(' AND ', $conditions);

        try {
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($params);

            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            die("Query error: " . $e->getMessage());
        }
    }

    /**
     * @param string $table
     * @param int $id
     * @param array $data
     * @return int
     */
    public function updateRecordById(string $table, int $id, array $data): int
    {
        $setColumns = [];
        $params = [':id' => $id];

        foreach ($data as $column => $value) {
            $setColumns[] = "$column = :$column";
            $params[":$column"] = $value;
        }

        $setColumnsString = implode(', ', $setColumns);
        $sql = "UPDATE $table SET $setColumnsString WHERE id = :id";

        try {
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($params);

            return $stmt->rowCount();
        } catch (PDOException $e) {
            die("Update error: " . $e->getMessage());
        }
    }
}
