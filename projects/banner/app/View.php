<?php
namespace App;

class View
{
    public static function renderHtml(string $htmlFile, array $session = null): void
    {
        $htmlFilePath = '../views/' . $htmlFile;
        if (file_exists($htmlFilePath)) {
            if ($session) {
                foreach ($session as $key => $value) {
                    Session::write($key, $value);
                }
            }
            $htmlContent = file_get_contents($htmlFilePath);
            header("Content-Type: text/html; charset=utf-8");
            echo $htmlContent;
        } else {
            header("HTTP/1.0 404 Not Found");
            echo "File not found.";
        }
    }
}