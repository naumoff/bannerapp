<?php

namespace App\Services;

use App\Actions\FetchBannerViewLogAction;
use App\Actions\GetBannerViewAction;
use App\Actions\InsertBannerViewLogAction;
use App\Actions\UpdateBannerViewLogAction;

readonly class UpdateBannerLogService
{
    private GetBannerViewAction $getBannerViewAction;
    private FetchBannerViewLogAction $fetchBannerViewLogAction;
    private UpdateBannerViewLogAction $updateBannerViewLogAction;
    private InsertBannerViewLogAction $insertBannerViewLogAction;

    public function __construct()
    {
        $this->getBannerViewAction = new GetBannerViewAction();
        $this->fetchBannerViewLogAction = new FetchBannerViewLogAction();
        $this->updateBannerViewLogAction = new UpdateBannerViewLogAction();
        $this->insertBannerViewLogAction = new InsertBannerViewLogAction();
    }
    public function handle(): void
    {
        $bannerViewDTO = $this->getBannerViewAction->handle();
        [$id, $valuesToUpdate] = $this->fetchBannerViewLogAction->handle($bannerViewDTO);

        if ($id && $valuesToUpdate['views_count']) {
            $valuesToUpdate['views_count']++;
            $valuesToUpdate['view_date'] = date('Y-m-d H:i:s');
            $this->updateBannerViewLogAction->handle($id, $valuesToUpdate);
        } else {
            $this->insertBannerViewLogAction->handle($bannerViewDTO);
        }
    }
}