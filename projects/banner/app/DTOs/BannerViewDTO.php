<?php

namespace App\DTOs;

use DateTime;

readonly class BannerViewDTO
{
    /**
     * @param string $ipAddress
     * @param string $userAgent
     * @param string $pageUrl
     * @param int $viewsCount
     */
    public function __construct(
        public string $ipAddress,
        public string $userAgent,
        public string $pageUrl,
        public int $viewsCount,
    ) {}

    public function toArray(): array
    {
        return [
            'ip_address' => $this->ipAddress,
            'user_agent' => $this->userAgent,
            'page_url' => $this->pageUrl,
            'views_count' => $this->viewsCount,
        ];
    }
}