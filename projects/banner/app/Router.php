<?php

namespace App;

use App\Controllers\BannerController;

/**
 * Ugly router plug
 */
class Router
{
    public static function init(): void
    {
        $htmlFile = match($_SERVER['REQUEST_URI']) {
            '/banner1' => 'index1.html',
            '/banner2' => 'index2.html',
            default => 'fallback.html',
        };

        $bannerController = new BannerController();
        $bannerController->renderHtmlFile($htmlFile);
    }
}