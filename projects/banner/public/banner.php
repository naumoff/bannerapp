<?php

session_start();
class BannerRenderer
{
    public static function render(string $imagePath)
    {
        $type = "image/jpeg";
        header("Content-Type: $type");
        header("Content-Length: " . filesize($imagePath));
        readfile($imagePath);
    }
}

BannerRenderer::render($_SESSION['fileName']);


