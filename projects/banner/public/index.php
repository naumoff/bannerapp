<?php
session_start();

require '../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable('../');
$dotenv->load();
$dotenv->required(['DB_CONNECTION', 'DB_HOST', 'DB_PORT', 'DB_DATABASE', 'DB_USERNAME', 'DB_PASSWORD']);

spl_autoload_register(function(string $class){
    $root = dirname(__DIR__);
    $file = $root.'/'.str_replace('\\', '/', $class).'.php';
    $file = str_replace("/App/", "/app/", $file);
    if(is_readable($file)){
        require $file;
    }
});

\App\Router::init();