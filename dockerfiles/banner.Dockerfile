FROM php:8.2-fpm

RUN apt update && apt install -y git zip unzip tree vim supervisor
RUN docker-php-ext-install pdo pdo_mysql pcntl sockets

WORKDIR /home/banner

COPY ./projects/banner ./

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

RUN composer install --prefer-dist --no-interaction --no-scripts

EXPOSE 9000

CMD php-fpm