CREATE DATABASE IF NOT EXISTS `banner`;

USE `banner`;

CREATE TABLE IF NOT EXISTS `banner_views` (
    id INT AUTO_INCREMENT PRIMARY KEY,
    ip_address VARCHAR(45) NOT NULL,
    user_agent TEXT NOT NULL,
    view_date DATETIME DEFAULT CURRENT_TIMESTAMP,
    page_url VARCHAR(255) NOT NULL,
    views_count INT NOT NULL
);

CREATE INDEX idx_ip_address_page_url ON banner_views (ip_address, page_url);